package com.zuckay.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by 1002473 on 2017. 1. 9..
 */
@RestController
public class HelloController {

    @RequestMapping("/")
    public String hello() {
        return "hello";
    }
}
